# Schneiden und gravieren von Vektorgrafiken mit dem Mr. Beam
Ich bin öfters mal auf der ein oder anderen Fahrraddemo unterwegs. Dafür wollte ich mir gerne ein Demoschild für mein Fahrrad lasern und dabei erklären, wie man den Laser verwendet.

## Ziel und Requirements festlegen
Als Erstes müssen wir uns ein Ziel festlegen. Ich möchte ein Demoschild aus Holz haben, welches ich seitlich an den Stangen von meinem Gepäckträger befestigen kann. Es soll dauerhaft befestigt sein. Daher sollte ich möglichst auch noch gelegentlich meine Fahrradtasche darüber fest machen können.

Ich möchte für das Schild sowohl etwas in das Holz hinein gravieren als auch das Holz auf die passende Größe zuschneiden und mit Befestigungslöchern versehen.

Da wir das gerade da haben, habe ich mich dazu entschieden 3 mm Birkensperrholz zu verwenden.


## Dateien vorbereiten
Um etwas auszuschneiden brauchen wir eine Vektorgrafik. Der Mr. Beam nimmt folgende Vektorformate an:
- SVG (Scalable Vector Graphics, wird oft für Icons verwendet)
- DXF (Drawing Interchange Format, wird für technische Zeichnungen verwendet)

Normalerweise würde ich einfach in Inkscape eine SVG vorbereiten, aber in diesem Fall brauche ich die genauen Dimensionen von meinem Fahrrad. Daher habe ich erstmal in Onshape einen Grundriss für das Schild gezeichnet.

### Technische Zeichnung in Onshape
Als Erstes habe ich ein Bild von meinem Gepäckträger gemacht und die wichtigsten Maße genommen, sodass ich das Bild in Onshape auf die richtigen Dimensionen skalieren kann:

![Gepäckträger](gepäckträger.jpg)

Dann habe ich so grob π mal Daumen die Grundform von meinem Schild eingezeichnet und Befestigungslöcher hinzugefügt, damit ich das Schild später mit Kabelbindern festzurren kann.

![Onshape Zeichnung](onshape_zeichnung.png)


### Gravur in Inkscape hinzufügen
Die Onshape Zeichnung habe ich als DXF (2018) exportiert und dann wieder in Inkscape importiert.

Ich habe im Internet ein cooles Logo gefunden, welche ich gerne auch auf meinem Schild haben möchte. Zusammen mit dem Text "Verkehrswende Jetzt!". Das Logo war im PNG Format, daher habe ich erst das Logo importiert und dann `Rechtsklick > Trace Bitmap`, um aus der Rastergrafik einen Vektor zu machen.

Den Text habe ich in einer schönen Schriftart hinzugefügt und alles passend auf der Grundplatte positioniert.

Alle Kanten, die später ausgeschnitten werden sollen, habe ich eine rote Linienfarbe gegeben. Dadurch kann ich sie später von den Gravuren unterscheiden. **Jetzt musste ich nur noch den Text in einen Pfad umwandeln, damit er vom Mr Beam erkannt wird** und natürlich habe ich die Datei als SVG abgespeichert.

![SVG von Inkscape](demoschild_fahrad.svg)

## Mr Beam bedienen
Als Erstes muss der Mr. Beam eingeschaltet werden. Dafür den Schlüssel an der Rückseite drehen und einmal auf den Knopf drücken. Anschließend öffnen wir die Klappe des Lasercutters

![Schlüssel drehen](mr_beam_schlüssel.jpg)

![Knopf Drücken](mr_beam_knopf.jpg)

Nach kurzem Warten ist das Webinterface vom Mr Beam unter http://192.168.178.43/ erreichbar. Hier müssen wir uns erstaml einloggen. Danach können wir auf `File` und dann auf `Upload` klicken um die SVG hochzuladen. Nach dem Hochladen erscheint die Datei rechts in der Dateiauswahl des Mr. Beams.

![Dateiauswahl](mr_beam_dateiauswahl.png)

Bevor wir die Datei richtig auf unserem Werkstück positionieren können, müssen wir das Holz überhaupt erstmal einlegen. Ich benutze gerne einen rechteckigen Gegenstand, um das Brett gerade an der Kante des Mr. Beams auszurichten.

Auf dem Kamerabild des Webinterfaces sollten wir jetzt das eingelegte Brett sehen können. Achtung: Die Kamera ist nur an, wenn die Klappe des Lasercutters geöffnet ist.

![SVG auf Brett](mr_beam_svg_auf_brett.png)

Ich habe direkt meine SVG auf das Brett verschoben. Da ich die Größe passend designt habe, ändere ich die Größe der SVG jetzt nicht mehr. Wenn man aber nur ein Logo lasern möchte, könnte man hier jetzt einfach noch die Größe und auch Rotation anpassen. **Wichtig: Das Bild vom Brett ist nicht ganz passend alligned und kann gut mal 5 mm nach oben oder unten verschoben. Deshalb lasse ich immer einen kleinen Rand.** 

Wenn alles richtig positioniert ist, können wir die Klappe schließen und auf Laser klicken.

Als Erstes stelle ich den Holztyp ein. Weil wir das gerade da hatten, verwende ich 3 mm Birch Plywood.


Im Laser-Menü wurde fast alles richtig erkannt: Die schwarzen Flächen sollen graviert werden, die roten Linien sollen ausgeschnitten werden. Eine Sache ist noch falsch. Anscheinen gibt es irgendwo noch eine schwarze Linie, die auch ausgeschnitten werden soll. Um solche Fehler zu erkennen, habe ich extra alle zu schneidenden Linien rot gemacht. Die schwarze Linie ziehe ich deshlab in den bereich `Skip`, um sie zu ignorieren.

![Skip black line](mr_beam_skip_black_line.png)

Als Nächstes ändere ich den Gravier-Modus. Dafür setze ich das Häkchen bei `show advanced settings` und ändere den `Engraving time optimization mode`auf `Fast`.

Die Einstellungen sind fertig, jetzt muss nur noch der Laser fokussiert werden. Dafür doppelklicke ich eine auf eine Stelle vom Werkstück, sodass Mr. Beam seinen Laser dorthin fährt. Ich Löse die Schraube an der Seite des Laserkopfes so weit, bis ich diesen nach oben und unten verschieben kann. Dann stelle ich das Fokus-Tool unter den Laser und schiebe den selbigen so bis zum Anstoß nach unten und schraube ihn wieder fest. Jetzt noch das Tool unter dem Laser herausziehen.

!(Laser Fokussieren)[mr_beam_laser_fokusieren.jpg]

Nun muss ich nur noch auf `start` klicken, noch einmal den Button am Mr. Beam zur Bestätigung drücken, in meinem Fall ca. 40 Minuten warten und Zack Fertig: Cooles Schild fürs Fahrrad.

![Fertig gelasertes Schild](mr_beam_schild_fertig.jpg)