# WIKI
Das hier ist unser WIKI fürs KI Makerspace. Hier gibt Anleitungen zu den Geräten. Wenn du eine Anleitungs vermisst, dann füge sie doch einfach selber hinzu. Wir verwenden einfaches Markdown für alle Dokumente. Wenn du Hilfe dabei brauchst, helfen wir dir gerne.

## Wikiartikel
- [Schneiden und gravieren von Vektorgrafiken mit dem Mr. Beam](Mr_Beam/Schneiden_und_Gravieren_von_Vectorgrafiken.md)
